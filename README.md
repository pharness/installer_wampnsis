# Installer_WAMPNSIS README #

### What is this repository for? ###

Custom installer for WAMP (Windows, Apache, MySQL, PHP) designed to be extended for your project's use. Installs WAMP, phpMyAdmin, and has examples of MSI install, default database creation, default user insertion.

* Setup_Suite.exe is built from the sources included and will install for a quick demo of what it can do. 

### What's Installed ###

* Apache 2.2.18, 32bit
* MySQL 5.1.57, 32 or 64bit as appropriate
* MySQL ODBC Connector 3.51.28, 32 or 64bit as appropriate
* PHP 5.3.6, 32bit
* VC Redistributable 2008SP1, 32 or 64bit as appropriate
* phpMyAdmin 3.3.7
* eSpeak 
* ffmpeg 

### Defaults ###

* MySQL root password: rootpass
* http://127.0.0.1:80
* C:\SuiteDir\...

### How do I get set up? ###

* Install NSIS, Nullsoft Scriptable Install System, v2.46
* Last notes I had say it does not work with NSIS 3.0b1
* Load and build InstallScript.nsi
* Change Suite, SuiteCompany, SuiteDir, etc to appropriate names for your project.

### Contribution guidelines ###

* I consider this version 1.0007 of my installer base, and have moved to a WIX based install system. I am not developing with/on this, but if you have fixes or improvements I'll gladly pull them in to help give back to the open source world.
* Version 3: [bitbucket.org/pharness/installer_wampwix](https://bitbucket.org/pharness/installer_wampwix)